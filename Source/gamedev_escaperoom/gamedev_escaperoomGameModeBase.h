// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "gamedev_escaperoomGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GAMEDEV_ESCAPEROOM_API Agamedev_escaperoomGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
